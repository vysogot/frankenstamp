import React from 'react';
import { Image, StyleSheet, View } from 'react-native';

import LoginScreen from './screens/Login';
import LoginReminderScreen from './screens/LoginReminder';
import HomeScreen from './screens/Home';
import DetailsScreen from './screens/Details';
import StampersListScreen from './screens/StampersList';

import { createStackNavigator } from 'react-navigation';

const RootStack = createStackNavigator(
  {
    Home: HomeScreen,
    StampersList: StampersListScreen,
    Details: DetailsScreen,
    Login: LoginScreen,
    LoginReminder: LoginReminderScreen,
  },
  {
    initialRouteName: 'Login',
    navigationOptions: {
      headerStyle: {
        backgroundColor: '#f4511e',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  }
);

export default class App extends React.Component {
  state = {
    isReady: false,
  };

  componentDidMount() {
    setTimeout(
      function () {
          this.setState({isReady: true});
      }
      .bind(this),
      1000
    );
  }

  render() {
    if (!this.state.isReady) {
      return (
        <View style={styles.container}>
          <Image
          source={require("./assets/frank.png")}
          style={styles.image}
          resizeMode="contain"
          ></Image>
        </View>
      );
    }

    return <RootStack />;
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  },
  image: {
    maxHeight: '80%',
    maxWidth: '80%'
  }
});