import React, { Component } from 'react';
import { View, StyleSheet, TextInput } from 'react-native';
import { MapView } from 'expo';

export default class HomeScreen extends Component {

  static navigationOptions = {
    title: 'Home',
    headerStyle: {
      display: 'none',
    }
  };

  render() {
    return (
      <MapView
        style={{ flex: 1 }}
        initialRegion={{
          latitude: 46.4208200,
          longitude: 6.2701000,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        }}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 30,
    backgroundColor: '#ecf0f1',
  }
});