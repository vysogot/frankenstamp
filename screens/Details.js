import React, { Component } from 'react';
import { Text, View, Image, StyleSheet } from 'react-native';
import { Constants } from 'expo';

export default class DetailsScreen extends React.Component {
  
  static navigationOptions = ({ navigation, navigationOptions }) => {
    return {
      headerTitle: navigation.getParam('name'),
      headerStyle: {
        backgroundColor: navigationOptions.headerTintColor,
      },
      headerTintColor: navigationOptions.headerStyle.backgroundColor,
    };
  };
  
  render() {
    
    const navigation = this.props.navigation;
    const params = navigation.state.params;
    
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Image style={ styles.logo } source={{ uri: params.avatarUrl }} />
        <Text>{params.subtitle}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  logo: {
    marginTop: 12,
    marginBottom: 12,
    backgroundColor: "#056ecf",
    height: 128,
    width: 128,
  }
});