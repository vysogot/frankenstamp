import React, { Component } from 'react';
import { View, StyleSheet, Image } from 'react-native';
import { Button, SocialIcon } from 'react-native-elements';

var notImplemented = require('../helpers/notImplemented.js');

export default class LoginScreen extends Component {

  static navigationOptions = {
    headerStyle: {
      display: 'none'
    }
  };

  render() {
    return (
        <View style={styles.container}>
        <Image
          style={styles.image}
          source={require('../assets/homeFrank.png')}
          resizeMode="contain"
        />
        <SocialIcon
          onPress={notImplemented('Facebook Login')}
          title='Login with Facebook'
          style={[styles.button, styles.facebook]}
          button
          type='facebook'
        />
        <SocialIcon
          onPress={notImplemented('Google+ Login')}
          title='Login with Google+'
          style={[styles.button, styles.google]}
          button
          type='google-plus-official'
        />
        <Button
          onPress={() => {
            this.props.navigation.navigate('LoginReminder')
          }}
          buttonStyle={[styles.button, styles.guest]}
          textStyle={styles.guestTitle}
          title="use without logging in"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#FFF'
  },
  image: {
    maxWidth: 290,
    maxHeight: 250,
    marginTop: 25,
    marginBottom: 15
  },
  button: {
    width: 300,
    height: 50,
    marginBottom: 15
  },
  guestTitle: {
    fontSize: 20,
    fontWeight: '700',
    color: 'black'
  },
  facebook: {
    backgroundColor: '#4b7ebd',
  },
  google: {
    backgroundColor: '#fd5d51'
  },
  guest: {
    backgroundColor: 'white'
  }
});