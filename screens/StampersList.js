import React, { Component } from 'react';
import { Text, StyleSheet } from 'react-native';

import { List, ListItem } from 'react-native-elements';

const list = [
  {
    id: 1,
    name: 'Marhaba',
    avatar_url: 'http://icons.iconarchive.com/icons/raindropmemory/down-to-earth/128/G12-3D-icon.png',
    subtitle: 'Pięć pieczątek i tortilla za friko z sosem'
  },
  {
    id: 2,
    name: 'Roti',
    avatar_url: 'http://icons.iconarchive.com/icons/raindropmemory/down-to-earth/128/G12-Flowerpot-Grass-icon.png',
    subtitle: 'Hot-dog szwedzki luj przy trzeciej pieczęci'
  },
  {
    id: 3,
    name: 'AliBaba',
    avatar_url: 'http://icons.iconarchive.com/icons/raindropmemory/down-to-earth/128/G12-Flower-2-icon.png',
    subtitle: 'Wysmaruj się sosem i wygraj kebaba'
  },
  {
    id: 6,
    name: 'Sphinx',
    avatar_url: 'http://icons.iconarchive.com/icons/raindropmemory/down-to-earth/128/G12-Dropbox-icon.png',
    subtitle: 'Dwie pieczątki za zakupy na bani'
  },
  {
    id: 7,
    name: 'Mak',
    avatar_url: 'http://icons.iconarchive.com/icons/raindropmemory/down-to-earth/128/G12-Finder-icon.png',
    subtitle: 'Dzisięć pieczątek małe frytki bez keczupu'
  },
  {
    id: 8,
    name: 'Radex',
    avatar_url: 'http://icons.iconarchive.com/icons/raindropmemory/down-to-earth/128/G12-Calculator-icon.png',
    subtitle: 'Brak aktualnych promocji oprócz czereśni'
  },
];

export default class StampersListScreen extends Component {
  
  static navigationOptions = {
    title: 'Stampers List',
  };
  
  
  render() {
    return (
      <List containerStyle={{marginBottom: 20}}>
      {
        list.map((l, i) => (
          <ListItem
            roundAvatar
            avatar={{uri:l.avatar_url}}
            key={i}
            title={l.name}
            subtitle={
              <Text style={styles.subtitle}>{l.subtitle}</Text>
            }
            onPress={() => {
            this.props.navigation.navigate('Details', {
              id: l.id,
              name: l.name,
              subtitle: l.subtitle,
              avatarUrl: l.avatar_url,
            });
          }}
          />
        ))
      }
    </List>
    );
  }
}

const styles = StyleSheet.create({
  subtitle: {
    paddingLeft: 10,
    color: 'grey'
  }
});