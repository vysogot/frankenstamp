module.exports = function notImplemented(what) {
    return () => alert('Hi, ' + what + ' feature is not yet implemented');
}